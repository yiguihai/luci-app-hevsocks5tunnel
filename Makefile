include $(TOPDIR)/rules.mk

PKG_VERSION:=1.0
PKG_RELEASE:=1

LUCI_TITLE:=LuCI for HevSocks5Tunnel
LUCI_DESCRIPTION:=Provides Luci for HevSocks5Tunnel
LUCI_DEPENDS:=+kmod-tun +coreutils-base64 +curl +perl
LUCI_PKGARCH:=all

define Package/$(PKG_NAME)/config
# shown in make menuconfig <Help>
help
	$(LUCI_TITLE)
	Version: $(PKG_VERSION)-$(PKG_RELEASE)
endef

include ../../luci.mk

# call BuildPackage - OpenWrt buildroot signature
