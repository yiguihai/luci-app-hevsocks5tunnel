'use strict';
'require fs';
'require uci';
'require form';
'require view';
'require poll';
'require rpc';
'require ui';
'require dom';

var conf = 'hevsocks5tunnel';
var callServiceList = rpc.declare({
    object: 'service',
    method: 'list',
    params: ['name'],
    expect: {
        '': {}
    }
});
var pollAdded = false;

function getServiceStatus() {
    return L.resolveDefault(callServiceList(conf), {})
        .then(function(res) {
            var is_running = false;
            try {
                is_running = res[conf]['instances']['hevsocks5tunnel']['running'];
            } catch (e) {}
            return is_running;
        });
}

function hevsocks5tunnelServiceStatus() {
    return Promise.all([
        getServiceStatus()
    ]);
}

function parseUrl(str) {
    var obj = {};
    var arr = str.split(';');
    for (var i = 0; i < arr.length; i++) {
        var parts = arr[i].split('=');
        obj[parts[0]] = parts[1];
    }
    return obj;
}

function hevsocks5tunnelRenderStatus(res) {
    var renderHTML = "";
    var isRunning = res[0];

    var autoSetDnsmasq = uci.get_first('hevsocks5tunnel', 'hevsocks5tunnel', 'auto_set_dnsmasq');
    var dnsmasqServer = uci.get_first('dhcp', 'dnsmasq', 'server');
    var forward = uci.get_first('firewall', 'defaults', 'forward');

    if (isRunning) {
        renderHTML += "<span style=\"color:green;font-weight:bold\">HevSocks5Tunnel - " + _("RUNNING") + "</span>";
    } else {
        renderHTML += "<span style=\"color:red;font-weight:bold\">HevSocks5Tunnel - " + _("NOT RUNNING") + "</span>";
        return renderHTML;
    }
    if (autoSetDnsmasq === '1') {
        var matchLine = "127.0.0.1#65353";

        //uci.unload('dhcp');
        //uci.load('dhcp');
        if (dnsmasqServer != undefined && dnsmasqServer.indexOf(matchLine) < 0) {
            uci.set('dhcp', '@dnsmasq[0]', 'server', matchLine);
            uci.save('dhcp');
        }
    }
    if (forward != undefined || forward == 'DROP' || forward == 'REJECT') {
        uci.set('firewall', '@defaults[0]', 'forward', 'ACCEPT');
        uci.save('firewall');
    };
    uci.apply();
    return renderHTML;
}

function traffic(i = 0) {
    let g = 0;
    const units = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB", "BB"];
    const power = 1024;
    let idx = 0;

    while (i >= power && idx < units.length) {
        g = (i / power).toFixed(2);
        i = Math.floor(i / power);
        idx++;
    }

    return `${g} ${units[idx]}`;
}

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function update_subscribe(json) {
    let ss = json['servers'];
    for (var i = 0; i < ss.length; i++) {
        let sid = uci.add('hevsocks5tunnel', 'shadowsocks');
        if (sid != undefined) {
            uci.set('hevsocks5tunnel', sid, 'id', ss[i]['id']);
            uci.set('hevsocks5tunnel', sid, 'method', ss[i]['method']);
            uci.set('hevsocks5tunnel', sid, 'password', ss[i]['password']);
            uci.set('hevsocks5tunnel', sid, 'plugin', ss[i]['plugin']);
            uci.set('hevsocks5tunnel', sid, 'plugin_opts', ss[i]['plugin_opts']);
            uci.set('hevsocks5tunnel', sid, 'remarks', ss[i]['remarks']);
            uci.set('hevsocks5tunnel', sid, 'server', ss[i]['server']);
            uci.set('hevsocks5tunnel', sid, 'server_port', ss[i]['server_port']);
            //uci.set('hevsocks5tunnel', sid, 'mode', 'tcp_and_udp');
            //uci.set('hevsocks5tunnel', sid, 'disabled', '1');
        }
    }
    uci.save('hevsocks5tunnel');
    uci.apply();
    window.location.reload(true);
}

function remove_subscribe() {
    let ss_list = uci.sections('hevsocks5tunnel', 'shadowsocks');
    for (var i = ss_list.length; i >= 0; i--) {
        if (uci.get('hevsocks5tunnel', '@shadowsocks[' + i + ']', 'id') != undefined) {
            uci.remove('hevsocks5tunnel', uci.resolveSID('hevsocks5tunnel', '@shadowsocks[' + i + ']'));
        }
    }
    uci.save('hevsocks5tunnel');
    uci.apply();
    window.location.reload(true);
}

return view.extend({
    load: function() {
        return Promise.all([
            uci.load('dhcp'),
            uci.load('hevsocks5tunnel'),
            uci.load('firewall'),
            L.resolveDefault(fs.exec('sslocal', ['-h']), null),
            //L.resolveDefault(fs.exec('pidof', ['hev-socks5-tunnel']), null)
        ]);
    },
    render: function(stats) {
        var m, s, o;

        m = new form.Map('hevsocks5tunnel', _('HevSocks5Tunnel'));
        m.title = _("HevSocks5Tunnel");
        m.description = _("A tunnel over Socks5 proxy (tun2socks) for Unix.");

        s = m.section(form.NamedSection, '_status');
        s.anonymous = true;
        s.render = function(section_id) {
            var renderStatus = function() {
                return L.resolveDefault(hevsocks5tunnelServiceStatus()).then(function(res) {
                    var view = document.getElementById("service_status");
                    if (view == null) {
                        return;
                    }

                    view.innerHTML = hevsocks5tunnelRenderStatus(res);
                });
            }

            if (pollAdded == false) {
                poll.add(renderStatus, 1);
                pollAdded = true;
            }

            return E('div', {
                class: 'cbi-section'
            }, [
                E('div', {
                        id: 'service_status'
                    },
                    _('Collecting data ...'))
            ]);
        }

        var sslocal = false,
            ss_help;
        if (stats[3] && stats[3].code == 0) {
            var sslocal = true;
            var ss_help = stats[3].stdout.trim();
            //var json = JSON.parse(stats[3].stdout.trim());
            //let methods = help.substring(start, end).split(/,\s+/);
            //console.log(methods);
            //uci.set('hevsocks5tunnel', '@hevsocks5tunnel[0]', 'remote_dns');

            //status[k] = JSON.parse(stats[3].stdout.trim());
        }

        ////////////////
        // Basic;
        ////////////////
        s = m.section(form.TypedSection, "hevsocks5tunnel", _("Settings"), _("Main configuration for hev-socks5-tunnel"));
        s.anonymous = true;

        s.tab("settings", _("General Settings"));
        s.tab("tunnel", _('Tunnel Settings'));
        s.tab("socks5", _("Socks5 Settings"));
        s.tab("misc", _("Misc Settings"));
        s.tab("shadowsocks", _("Shadowsocks Settings"));

        ///////////////////////////////////////
        // Basic Settings
        ///////////////////////////////////////
        o = s.taboption("settings", form.Flag, "enabled", _("Enable"), _("Enable or disable Shadowsocks server"));
        o.rmempty = false;
        o.default = o.disabled;

        // route
        o = s.taboption("settings", form.ListValue, "route_list", _("Route"),
            _("Acl"));
        o.rmempty = false;
        o.default = "bypass_chn";
        o.placeholder = "default";
        o.value("all", _("All"));
        o.value("bypass_chn", _("Bypass mainland China"));
        o.value("gfwlist", _("GFW List"));

        // bypass user;
        o = s.taboption("settings", form.DynamicList, "bypass_user", _("User"),
            _("bypass skuid."));
        o.rmempty = false;
        o.default = "nobody";
        //o.placeholder = "default";
        o.value("root", _("root"));
        o.value("nobody", _("nobody"));

        // auto-conf-dnsmasq;
        o = s.taboption("settings", form.Flag, "auto_set_dnsmasq", _("Automatically Set Dnsmasq"), _("Automatically set as upstream of dnsmasq when port changes."));
        o.rmempty = false;
        o.default = o.enabled;
        o.depends({
            route_list: "all",
            "!reverse": true
        });
        o.readonly = true;

        // local dns;
        o = s.taboption("settings", form.Value, "local_dns", _("Local DNS"), _("Name of device name listen on."));
        o.datatype = "or(ipaddr, string)";
        o.rmempty = false;
        o.placeholder = "223.5.5.5#53";
        o.default = "223.5.5.5#53";
        o.depends({
            route_list: "all",
            "!reverse": true
        });

        // remote dns;
        o = s.taboption("settings", form.Value, "remote_dns", _("Remote DNS"), _("Name of device name listen on."));
        o.placeholder = "8.8.8.8#53";
        o.default = "8.8.8.8#53";
        o.datatype = "or(ipaddr, string)";
        o.rmempty = false;
        o.depends({
            route_list: "all",
            "!reverse": true
        });

        ///////////////////////////////////////
        // Tunnel Settings
        ///////////////////////////////////////

        // bind device name;
        o = s.taboption("tunnel", form.Value, "name", _("Interface name"), _("Interface name."));
        o.placeholder = "tun0";
        o.rmempty = false;
        o.datatype = "network";
        //o.readonly = true;
        o.default = "tun0";

        // mtu;
        o = s.taboption("tunnel", form.Value, "mtu", _("Interface MTU"), _("Interface MTU"));
        o.placeholder = 8500;
        o.default = 8500;
        o.datatype = "range(48, 65535)";
        o.rmempty = false;

        // multi-queue
        o = s.taboption("tunnel", form.Flag, "multi_queue", _("Multi-queue"), _("Multi-queue"));
        o.rmempty = false;
        o.default = o.disable;
        //o.readonly = true;

        // ipv4 address;
        o = s.taboption("tunnel", form.Value, "ipv4", _("IPv4 address"), _("IPv4 address."));
        o.rmempty = false;
        o.placeholder = "198.18.0.1";
        o.default = "198.18.0.1";
        o.datatype = "ip4addr";
        o.readonly = true;

        // ipv6 address;
        o = s.taboption("tunnel", form.Value, "ipv6", _("IPv6 address"), _("IPv6 address."));
        o.rmempty = false;
        o.placeholder = "fc00::1";
        o.default = "fc00::1";
        o.datatype = "ip6addr";
        o.readonly = true;

        ////////////////
        // Socks5
        ////////////////

        // port;
        o = s.taboption("socks5", form.Value, "port", _("Port"), _("Socks5 server port"));
        o.placeholder = 1080;
        o.default = 1080;
        o.datatype = "port";
        o.rmempty = false;

        // address;
        o = s.taboption("socks5", form.Value, "address", _("Address"), _("Socks5 server address (ipv4/ipv6)."));
        o.rmempty = false;
        o.placeholder = "127.0.0.1";
        o.default = "127.0.0.1";
        o.datatype = "ipaddr";

        // udp;
        o = s.taboption("socks5", form.ListValue, "udp", _("UDP"),
            _("Socks5 UDP relay mode (tcp|udp)"));
        o.rmempty = false;
        o.default = "udp";
        o.placeholder = "udp";
        o.value("tcp", _("TCP"));
        o.value("udp", _("UDP"));

        // username;
        o = s.taboption("socks5", form.Value, "username", _("Username"), _("Socks5 server username."));
        o.datatype = "string";

        // password;
        o = s.taboption("socks5", form.Value, "password", _("Password"), _("Socks5 server password."));
        o.datatype = "string";
        o.password = true;

        ////////////////
        // Misc
        ////////////////

        // task stack size;
        o = s.taboption("misc", form.Value, "task_stack_size", _("task-stack-size"), _("task stack size (bytes)"));
        o.placeholder = 20480;
        o.default = 20480;
        o.datatype = "uinteger";

        // connect timeout;
        o = s.taboption("misc", form.Value, "connect_timeout", _("connect-timeout"), _("connect timeout (ms)"));
        o.placeholder = 5000;
        o.default = 5000;
        o.datatype = "uinteger";

        // read-write timeout;
        o = s.taboption("misc", form.Value, "read_write_timeout", _("read-write timeout"), _("read-write timeout (ms)"));
        o.placeholder = 60000;
        o.default = 60000;
        o.datatype = "uinteger";

        // log;
        o = s.taboption("misc", form.Value, "log_file", _("log-file"), _("stdout, stderr or file-path"));
        o.placeholder = "stdout";
        o.default = "/dev/null";
        o.datatype = "string";
        o.readonly = true;

        // log-level;
        o = s.taboption("misc", form.ListValue, "log_level", _("log-level"), _("debug, info, warn or error"));
        o.placeholder = "warn";
        o.default = "error";
        //o.rmempty = false;
        //o.readonly = true;
        o.value("debug", _("debug"));
        o.value("info", _("info"));
        o.value("warn", _("warn"));
        o.value("error", _("error"));

        // pid file;
        o = s.taboption("misc", form.Value, "pid_file", _("pid-file"), _("If present, run as a daemon with this pid file"));
        o.placeholder = "/tmp/hev-socks5-tunnel.pid";
        o.default = "/tmp/hev-socks5-tunnel.pid";
        o.datatype = "string";
        //o.readonly = true;

        // limit nofile;
        o = s.taboption("misc", form.Value, "limit_nofile", _("limit-nofile"), _("If present, set rlimit nofile; else use default value"));
        o.placeholder = 65535;
        o.default = 65535;
        o.datatype = "uinteger";

        if (sslocal) {
            // shadowsocks
            var ss_list = uci.sections('hevsocks5tunnel', 'shadowsocks');
            o = s.taboption("shadowsocks", form.ListValue, "global_server", _("Main Server"),
                _("Main Server."));
            o.value("nil", _("Disable"));
            o.value("best", _("Balancer"));
            for (var i = 0; i < ss_list.length; i++) {
                //console.log(ss_list[i].disabled);
                if (ss_list[i].disabled != "0") {
                    o.value(ss_list[i].remarks, _(ss_list[i].remarks + ' ( ' + ss_list[i].server + ':' + ss_list[i].server_port + ' )'));
                }
            }

            o = s.taboption("shadowsocks", form.Value, "subscribe_url", _("Subscribe URL"), _("Subscribe URL."));
            o.rmempty = false;
            if (ss_help.indexOf('for tunnel') > 0) {
                o = s.taboption("shadowsocks", form.ListValue, "protocol", _("Tunnel configuration"),
                    _("Tunnel local server."));
                o.value("tunnel", _("tunnel"));
                o.default = "tunnel";
                o.readonly = true;

                // port;
                o = s.taboption("shadowsocks", form.Value, "local_port", _("Listen port"), _("Listen Port"));
                o.placeholder = 1081;
                o.default = 1081;
                o.datatype = "port";
                o.rmempty = false;
                o.readonly = true;

                // address;
                o = s.taboption("shadowsocks", form.Value, "local_address", _("Listen Address"), _("Listen address."));
                o.rmempty = false;
                o.placeholder = "127.0.0.1";
                o.default = "127.0.0.1";
                o.datatype = "ipaddr";
                o.readonly = true;

                // port;
                o = s.taboption("shadowsocks", form.Value, "forward_port", _("Forward port"), _("Listen Port"));
                o.placeholder = 1080;
                o.default = 1080;
                o.datatype = "port";
                //o.rmempty = false;

                // address;
                o = s.taboption("shadowsocks", form.Value, "forward_address", _("Forward address"), _("Forward address."));
                //o.rmempty = false;
                o.placeholder = "1.2.3.4";
                o.default = "1.2.3.4";
                o.datatype = "ipaddr";
                //o.readonly = true;
            }
            o = s.taboption("shadowsocks", form.ListValue, "mode", _("Mode"), _("Mode"));
            o.placeholder = "tcp_and_udp";
            o.value("tcp_and_udp", _("tcp_and_udp"));
            o.value("tcp_only", _("tcp_only"));
            o.value("udp_only", _("udp_only"));
            o.default = "tcp_only";
            o.readonly = true;

            // max_server_rtt
            o = s.taboption("shadowsocks", form.Value, "max_server_rtt", _("max_server_rtt"), _("MAX Round-Trip-Time (RTT) of servers <p>The timeout seconds of each individual checks</p>"));
            o.placeholder = 5;
            o.default = 5;
            o.datatype = "uinteger";
            o.depends("global_server", "best");

            // read-write timeout;
            o = s.taboption("shadowsocks", form.Value, "check_interval", _("check_interval"), _("Interval seconds between each check"));
            o.placeholder = 10;
            o.default = 10;
            o.datatype = "uinteger";
            o.depends("global_server", "best");

            // check_best_interval
            o = s.taboption("shadowsocks", form.Value, "check_best_interval", _("check_best_interval"), _("Interval seconds between each check for the best server <p>Optional. Specify to enable shorter checking interval for the best server only.</p>"));
            o.placeholder = 5;
            o.default = 5;
            o.datatype = "uinteger";
            o.depends("global_server", "best");

            o = s.taboption("shadowsocks", form.ListValue, "runtime", _("Runtime configuration"),
                _("single_thread or multi_thread."));
            o.value("single_thread", _("single_thread"));
            o.value("multi_thread", _("multi_thread"));
            o.default = "multi_thread";

            // read-write timeout;
            o = s.taboption("shadowsocks", form.Value, "worker_count", _("worker_count"), _("Worker threads that are used in multi-thread runtime"));
            o.placeholder = 10;
            o.default = 10;
            o.datatype = "uinteger";
            o.depends("runtime", "multi_thread");

            /*
            ////////////////
            // Shadowsocks;
            ////////////////
            s = m.section(form.TypedSection, "shadowsocks", _("Settings"), _("Main configuration for shadowsocks"));
            s.anonymous = true;

            s.tab("settings", _("General Settings"));
            
            // blacklist;
            o = s.taboption("settings", form.TextValue, "ss_conf",
            	"", _("Configure IP blacklists that will be filtered from the results of specific DNS server."));
            o.rows = 20;
            o.cfgvalue = function (section_id) {
            	return fs.trimmed('/etc/smartdns/blacklist-ip.conf');
            };
            o.write = function (section_id, formvalue) {
            	return this.cfgvalue(section_id).then(function (value) {
            		if (value == formvalue) {
            			return
            		}
            		return fs.write('/etc/smartdns/blacklist-ip.conf', formvalue.trim().replace(/\r\n/g, '\n') + '\n');
            	});
            };
            */

            ////////////////
            // Upstream servers;
            ////////////////

            s = m.section(form.GridSection, "shadowsocks", _("Upstream Servers"),
                _("Extended multiple server configuration"));
            s.anonymous = true;
            s.addremove = true;
            s.sortable = true;

            s.tab('servers', _('General Settings'));

            // enable flag;
            o = s.taboption("servers", form.Flag, "disabled", _("Enable"), _("Enable"));
            o.rmempty = false;
            o.default = o.enabled;
            o.editable = true;
            o.modalonly = false;

            // name;
            o = s.taboption("servers", form.Value, "remarks", _("Remark"), _("Server Name"));

            // address;
            o = s.taboption("servers", form.Value, "server", _("Address"), _("Server Address.</p>"));
            o.datatype = "or(ipaddr, string)";
            o.rmempty = false;
            /*
              o.render = function(section_id, values) {
              	console.log(values);
                  if (values !== undefined) {
                              // 获取表格元素
                              var table = document.querySelector(".table.cbi-section-table");
                              // 获取所有的单元格元素
                              var cells = table.querySelectorAll(".td.cbi-value-field");
                              // 遍历每个单元格
                              for (var i = 0; i < cells.length; i++) {
                                  // 获取单元格的文本内容
                                  var text = cells[i].textContent;
                                  // 如果文本内容是2233，就替换成888
                                  if (text == "-1") {
                                      //cells[i].textContent = "<i>888<i/>";
                                      cells[i].innerHTML = "<i>" + 666 + "<i/>";
                                  }
                                  //console.log(cells[i].innerHTML+"\n");
                              }
                          }
              };
              */

            // port;
            o = s.taboption("servers", form.Value, "server_port", _("Port"), _("Server Port"));
            o.datatype = "port";
            o.rmempty = false;

            // method;
            o = s.taboption("servers", form.ListValue, "method", _("Method"), _("Encryption Method"));
            o.placeholder = "2022-blake3-aes-256-gcm";
            let start = ss_help.indexOf("Server's encryption method [possible values: ") + "Server's encryption method [possible values: ".length;
            let end = ss_help.indexOf("]", start);
            let encryption_method = ss_help.substring(start, end).replace("\n", " ").split(/,\s*/);
            encryption_method.forEach(element => {
                //ui.addNotification(null, E('p', element), 'error')
                o.value(element.toLowerCase(), _(element.toUpperCase()));
            });
            o.default = "aes-128-gcm";
            o.rmempty = false;

            // password;
            o = s.taboption("servers", form.Value, "password", _("Password"), _("Password"));
            o.datatype = "string";
            o.password = true;
            o.modalonly = true;
            o.rmempty = false;

            encryption_method.forEach(element => {
                if (element !== 'plain' && element !== 'none') {
                    o.depends("method", element);
                }
            })

            // mode;
            o = s.taboption("servers", form.ListValue, "mode", _("Mode"), _("Mode"));
            o.placeholder = "tcp_and_udp";
            o.value("tcp_and_udp", _("tcp_and_udp"));
            o.value("tcp_only", _("tcp_only"));
            o.value("udp_only", _("udp_only"));
            o.default = "tcp_and_udp";
            o.readonly = true;
            o.modalonly = true;

            // plugin;
            o = s.taboption("servers", form.Value, "plugin", _("Plugin"), _("Plugin."));
            o.value("v2ray-plugin", _("v2ray-plugin"));
            o.value("kcptun", _("kcptun"));
            o.value("obfs-local", _("obfs-local"));

            // plugin_opts;
            o = s.taboption("servers", form.Value, "plugin_opts", _("Plugin Opts"), _("Plugin Opts."));
            o.modalonly = true;
            o.depends({
                plugin: null,
                "!reverse": true
            });

            // ping;
            o = s.taboption("servers", form.HiddenValue, "ping_latency", _("Ping Latency"), _("ms"));
            o.datatype = "uinteger";
            o.readonly = true;
            o.modalonly = false;
            //o.default = -1;
            //https://github.com/openwrt/luci/blob/8a2ace10d6f59a4afc388ffbe2076cda10bf49c9/applications/luci-app-vnstat2/htdocs/luci-static/resources/view/vnstat2/config.js#L78
            /*
            o.load = function(section_id) {
                var addre = uci.get('hevsocks5tunnel', section_id, 'server');
                var disablede = uci.get('hevsocks5tunnel', section_id, 'disabled');
                //row = document.querySelector('.cbi-section-table-row[data-sid="%s"]'.format(section_id));
                //element = row.querySelector('.cbi-value-field');
                //element.textContent = -1;
                //row.setAttribute('style', 'opacity: 0.5; color: #37c !important;');
                if (disablede != 0) {
                    return fs.exec('ping', ['-W', '2', '-c', '1', addre]).then(L.bind(function(result) {
                        if (result.code == 0) {
                            var index = result.stdout.indexOf("time="); // find the index of "time="
                            var time = result.stdout.substring(index + 5, index + 11); // get the substring from index to index + 14
                            if (time) {
                                return time;
                            }
                        }
                    }, this));
                }
            };
            */

            o = s.taboption("servers", form.HiddenValue, "connected", _("Socket Connected"), _("Socket Connected."));
            o.datatype = "string";
            o.readonly = true;
            o.modalonly = false;
            //o.default = "N/A";
        };

        ////////////////
        // Support
        ////////////////
        s = m.section(form.TableSection, "manual", _("Technical Support"),
            _("If you like this software, please buy me a cup of coffee."));
        s.anonymous = true;

        o = s.option(form.Button, "web");
        o.title = _("SmartDNS official website");
        o.inputtitle = _("open website");
        o.inputstyle = "apply";
        o.onclick = function() {
            window.open("https://pymumu.github.io/smartdns", '_blank');
        };

        if (sslocal) {
            o = s.option(form.Button, "update_sub");
            o.title = _("Update All Subscribe Servers");
            o.inputtitle = _("Update");
            o.inputstyle = "add";
            o.onclick = function() {
                let sub_url = uci.get_first('hevsocks5tunnel', 'hevsocks5tunnel', 'subscribe_url');
                if (sub_url != undefined) {
                    o.readonly = true;
                    fs.exec('curl', ['--silent', '--location', sub_url]).then(function(result) {
                        if (result.stdout && result.code == 0) {
                            var sub_json = JSON.parse(result.stdout.trim());
                            if (typeof sub_json === 'object') {
                                update_subscribe(sub_json);
                            }
                        }
                    });
                }
            };
            o = s.option(form.Button, "delete");
            o.title = _("Delete All Subscribe Servers");
            o.inputtitle = _("Delete");
            o.inputstyle = "remove";
            o.onclick = function() {
                remove_subscribe();
            };
            o = s.option(form.Button, "check");
            o.title = _("One-click connection check");
            o.inputtitle = _("Check");
            o.inputstyle = "action";
            o.onclick = function() {
                let table = document.querySelector('.table.cbi-section-table');
                let ss_list = uci.sections('hevsocks5tunnel', 'shadowsocks');
                for (let i = ss_list.length; i >= 0; i--) {
                    let sid = uci.resolveSID('hevsocks5tunnel', '@shadowsocks[' + i + ']');
                    if (sid != undefined) {
                        let disabled = uci.get('hevsocks5tunnel', sid, 'disabled');
                        let remarks = uci.get('hevsocks5tunnel', sid, 'remarks');
                        let server = uci.get('hevsocks5tunnel', sid, 'server');
                        let server_port = parseInt(uci.get('hevsocks5tunnel', sid, 'server_port'), 10);
                        let method = uci.get('hevsocks5tunnel', sid, 'method');
                        let password = uci.get('hevsocks5tunnel', sid, 'password');
                        let plugin = uci.get('hevsocks5tunnel', sid, 'plugin');
                        let plugin_opts = uci.get('hevsocks5tunnel', sid, 'plugin_opts');
                        let randomNumber = getRandomInt(1024, 65535);
                        let config = {
                            server: server,
                            server_port: server_port,
                            password: password,
                            method: method,
                            plugin: plugin,
                            plugin_opts: plugin_opts,
                            local_address: "127.0.0.1",
                            local_port: randomNumber
                        };

                        let json = JSON.stringify(config);

                        let cell = table.querySelector('tr[data-sid="%s"]'.format(sid));
                        if (disabled != 0) {
                            let time = fs.exec('ping', ['-W', '2', '-c', '1', server]).then(function(result) {
                                //console.log(cell);
                                if (result.code == 0) {
                                    let index = result.stdout.indexOf("time="); // find the index of "time="
                                    let time = result.stdout.substring(index + 5, index + 11); // get the substring from index to index + 14
                                    if (time) {
                                        let col = '#ff0000';
                                        if (time < 300) col = '#ff3300';
                                        if (time < 200) col = '#ff7700';
                                        if (time < 100) col = '#249400';
                                        return cell.querySelector("td[data-name='ping_latency']").innerHTML = `<font color="${col}">${(time ? time : "--") + " ms"}</font>`;
                                    }
                                }
                            });
                            let connect = cell.querySelector("td[data-name='connected']");
                            let websocket = false,
                                protocol, plugin_opt;
                            if (plugin_opts && plugin_opts.length > 0) {
                                plugin_opt = parseUrl(plugin_opts);
                                if (plugin == 'v2ray-plugin' && plugin_opt['mode'] === undefined) {
                                    websocket = true;
                                }
                            }
                            if (websocket) {
                                (plugin_opts.indexOf('tls') > 0) ? protocol = 'https://': protocol = 'http://';
                                let address = protocol + plugin_opt['host'] + '/' + plugin_opt['path'];
                                let socket = fs.exec('curl', ['--http1.1', '-m', '2', '-ksN', '-o', '/dev/null', '-w', 'time_connect=%{time_connect}\\nhttp_code=%{http_code}', '-H', 'Connection: Upgrade', '-H', 'Upgrade: websocket', '-H', 'Sec-WebSocket-Key: SGVsbG8sIHdvcmxkIQ==', '-H', 'Sec-WebSocket-Version: 13', address]).then(function(result) {
                                    if (result.code == 28) {
                                        let e = {};
                                        e.socket = result.stdout.match(/http_code=(\d+)/)[1] === "101";
                                        e.ping = parseFloat(result.stdout.match(/time_connect=(\d+.\d{3})/)[1]) * 1000;
                                        if (e.socket) {
                                            connect.innerHTML = '<font color="#249400">ok</font>';
                                        } else {
                                            connect.innerHTML = '<font color="#ff0000">fail</font>';
                                        }
                                    }
                                });
                            } else {
                                fs.write('/tmp/ss_' + randomNumber + '.json', json);
                                let ss = fs.exec('sslocal', ['--config', '/tmp/ss_' + randomNumber + '.json', '--user', 'nobody', '--daemonize-pid', '/tmp/ss_' + randomNumber + '.pid', '--daemonize']).then(function(result) {
                                    if (result.code == 0) {
                                        let ss2 = fs.exec('curl', ['--silent', '--proxy', 'socks5://127.0.0.1:' + randomNumber, '--output', '/dev/null', '--write-out', '%{response_code}', '--max-time', '3', '--resolve', 'cp.cloudflare.com:443:104.16.133.229', '--resolve', 'cp.cloudflare.com:80:104.16.133.229', '--url', 'http://cp.cloudflare.com/generate_204']).then(function(result) {
                                            let ss3 = fs.exec('sh', ['-c', 'kill $(cat /tmp/ss_' + randomNumber + '.pid)']).then(function(result) {
                                                if (result.code == 0) {
                                                    fs.remove('/tmp/ss_' + randomNumber + '.pid');
                                                }
                                            });
                                            if (result.code == 0) {
                                                if (result.stdout == 204) {
                                                    connect.innerHTML = '<font color="#249400">ok</font>';
                                                } else {
                                                    connect.innerHTML = '<font color="#ff0000">fail</font>';
                                                }
                                            }
                                        });
                                    }
                                    fs.remove('/tmp/ss_' + randomNumber + '.json');
                                });
                            }
                        }
                    }
                }
            };
        }
        o = s.option(form.DummyValue, "_restart", _("Restart Service"));
        o.renderWidget = function() {
            return E('button', {
                'class': 'btn cbi-button cbi-button-reload',
                'id': 'btn_restart',
                'click': ui.createHandlerFn(this, function() {
                    return fs.exec('/etc/init.d/hevsocks5tunnel', ['restart'])
                        .catch(function(e) {
                            ui.addNotification(null, E('p', e.message), 'error')
                        });
                })
            }, [_("Restart")]);
        }

        return m.render();
    }
});
